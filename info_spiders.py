import time
from selenium import webdriver
import requests
from lxml import etree


class wuAiapp(object):
    def __init__(self):
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36'
        }
        self.urls = []

    def wuai(self, num=1):
        for i in range(1, num + 1):
            self.urls.append('https://www.52pojie.cn/forum-16-%s.html' % i)
            # print(self.urls)
        with open('wuai.org', 'a', encoding='utf8') as f:
            f.write('* 吾爱破解软件[%s]\n' % time.strftime('%Y-%m-%d %H:%M:%S'))
            for url in self.urls:
                responses = requests.get(url, headers=self.headers)
                # html_str = responses.content.decode('utf8')
                html_str = responses.text
                html = etree.HTML(html_str)
                xx = html.xpath('//table[@summary="forum_16"]/tbody/tr/th[@class="new"]/a[@class="s xst"]')
                for i in xx:
                    f.write('** ' + i.xpath('./text()')[0] + '\n')
                    # f.write('*** ' + 'https://www.52pojie.cn/' + i.xpath('./@href')[0] + '\n')
                    f.write('*** ' + '[[https://www.52pojie.cn/%s][原文链接]]' % i.xpath('./@href')[0] + '\n')


class jianShuApp(object):

    def __init__(self):
        self.driver = webdriver.Chrome(executable_path='./drvier/chromedriver')

    def jianShuInfo(self, url, temstr):
        try:
            self.driver.get(url)
            time.sleep(1)
            i = 0
            while i < 3:
                # print('循环1')
                self.driver.execute_script('window.scrollTo(0,document.body.scrollHeight)')  # 执行js的方法
                i += 1
                time.sleep(1)
            i2 = 0
            while i2 < 3:
                i2 += 1
                # print('循环2的i:' + str(i2))
                element2 = self.driver.find_elements_by_class_name('load-more')
                if len(element2) == 0:
                    break
                element2[0].click()
                time.sleep(1)

            # if i == 3:
            # print('写入')
            title = self.driver.find_elements_by_class_name('title')
            abstract = self.driver.find_elements_by_class_name('abstract')
            with open('wuai.org', 'a', encoding='utf8') as f:
                i3 = 1
                f.write('* 简书%s' % temstr + '\n')
                for (str1, str2) in zip(title, abstract):
                    f.write('** %s:' % str(i3) + str1.text + '\n')
                    f.write('- ' + str2.text + '\n')
                    f.write('*** ' + '[[%s][原文链接]]' % str1.get_attribute('href') + '\n')
                    i3 += 1

        except:
            print('简书bad')

        finally:
            # print('finally')
            self.driver.quit()


class thirtySix(object):

    def __init__(self):
        self.driver = webdriver.Chrome(executable_path='./drvier/chromedriver')
        self.url = 'https://www.36kr.com/newsflashes'

    def thirtySixInfo(self, num):
        try :
            i = 0
            while i < 2:
                self.driver.get(self.url)
                self.driver.execute_script('window.scrollTo(0,document.body.scrollHeight)')
                time.sleep(2)
                i += 1
            i2 = 0
            while i2 < int(num):
                tem_element = self.driver.find_elements_by_xpath("//*[@class='kr-loading-more-button show']")

                for temi in tem_element:
                    temi.click()
                time.sleep(1)
                self.driver.execute_script('window.scrollTo(0,document.body.scrollHeight)')
                time.sleep(3)
                i2 += 1

            title = self.driver.find_elements_by_xpath('//div[@class="flow-item"]/div[@class="item-main"]/div[@class="newsflash-item"]/a')
            desc = self.driver.find_elements_by_xpath('//div[@class="flow-item"]/div[@class="item-main"]/div[@class="newsflash-item"]/div[@class="item-desc"]')
            with open('wuai.org', 'w', encoding='utf8') as f:
                f.write('* 36氪快讯' + '\n')
                for (x1, x2) in zip(title, desc):
                    f.write('** %s' % x1.text + '\n')
                    f.write('- %s' % x2.text + '\n')
                    x3 = x2.find_elements_by_xpath('a')
                    if len(x3):
                        f.write('*** [[%s][原文链接]]' % x3[0].get_attribute('href') + '\n')



        finally:
            self.driver.quit()


#36氪
thirtySix().thirtySixInfo(num=4)

# 简书热点
jianshu = jianShuApp()
jianshu2 = jianShuApp()
url1 = 'https://www.jianshu.com/trending/monthly?utm_medium=index-banner-s&utm_source=desktop'
url2 = 'https://www.jianshu.com/trending/weekly?utm_medium=index-banner-s&utm_source=desktop'
jianshu2.jianShuInfo(url=url2, temstr='7天热门')
jianshu.jianShuInfo(url=url1, temstr='30天热门')

# 无爱破解软件
wuAiapp().wuai(num=3)




